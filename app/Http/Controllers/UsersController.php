<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Helpers\MPage;

class UsersController extends Controller
{

    protected function createUser(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|unique:users|min:2|max:32',
            'password' => 'required|string|min:5|max:48',
            'email' => 'required|string|unique:users|min:4|max:180',
            'phone' => 'string|min:10|max:16',
            'name' => 'required|string|min:2|max:32',
            'last_name' => 'required|string|min:2|max:32',
            'role' => 'in:usuario,admin'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $user = User::createUser($_request);
        if(!$user) return response()->json("Error de servisor",500);

        return response()->json(['Usuario Creado Exitosamente'],200);
    }


    protected function editUser($id,Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'string|min:2|max:32',
            'password' => 'string|min:5|max:48',
            'email' => 'string|min:4|max:180',
            'phone' => 'string|min:10|max:16',
            'name' => 'string|min:2|max:32',
            'last_name' => 'string|min:2|max:32',
            'role' => 'in:usuario,admin'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $_request = $request->all();
        $_request['user_id'] = $id;

        $user = User::editUser($_request);

        $user = User::find($id);

        return response()->json(['message' => 'Usuario Editado Exitosamente','user' => $user],200);
    }

    public function me(){
      $user = Auth::user();
      if (!$user) return response()->json('No conectado', 404);

      //var_dump($user->id);exit();

      $me = DB::table('users')->where('id', $user->id )->first();
      //var_dump($me);exit();
      if (!$me) return response()->json('Error de servidor', 500);

      return response()->json($me);
    }


    public function getUser($slug){
      $user = User::getUser($slug);
      if (!$user) return response()->json('Database Error', 500);
      return $user;
    }

    public function paginate(Request $request){

        $pquery = DB::table('users')->where('trash', 0);


        $users = null;

        try {
            $users = MPage::paginate($pquery, $request, 10, '', 'users');
        } catch(\Exception $e) {
            return response()->json('Error al obtener datos', 500);
        }

        return $users;
    }
}
