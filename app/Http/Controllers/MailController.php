<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\MailHelper;

class MailController extends Controller
{
    public function send(Request $request){
      $_request = $request->all();
      MailHelper::sendMail($_request,"Contacto Interesado");
      return response()->json('Gracias por contactarnos',200);
    }
}
