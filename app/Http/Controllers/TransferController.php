<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Transfer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Helpers\MPage;
use Carbon\Carbon;

class TransferController extends Controller
{

  protected function createTransfer(Request $request){
      $validator = Validator::make($request->all(), [
          'cantidad' => 'required|string',
          'referencia' => 'required|string',
          'banco' => 'required|in:banesco,venezuela,provincial',
          'cuenta' => 'required'
      ]);

      if($validator->fails()){
          return response()->json($validator->errors()->toJson(), 400);
      }

      $_request = $request->all();

      $transfer = Transfer::createTransfer($_request);
      if(!$transfer) return response()->json("Error de servidor",500);

      return response()->json('Gracias por donar!',200);
  }

  public function removeTransfer($id){
    $remove = Transfer::removeTransfer($id);
    return $remove;
  }

  protected function updateTransfer($id, Request $request){
    //A esto le faltan mas validaciones de seguridad xD
      $validator = Validator::make($request->all(), [
        'estado' => 'required'
      ]);

      if($validator->fails()){
          return response()->json($validator->errors()->toJson(), 400);
      }

      $_request = $request->all();

      $transfer = Transfer::find($id);

      if(!$transfer) return response()->json("Transferencia no encontrada",404);
      $transfer->estado = $_request['estado'];
      $transfer->save();
      return response()->json('Operacion exitosa',200);
  }

  public static function DateRange($query,$date,$param,$tableName){
    if(is_array($date)){
      $startDate=$date[0];
      $endDate=$date[1];

      return $query->whereBetween($tableName.'.'.$param,[$startDate,$endDate]);
      //where($tableName.'.'.$param, '>=', $startDate)
      //->where($tableName.'.'.$param, '<=', $endDate);
    }

    return $query->where($tableName.'.'.$param,'=',$date);
  }


  public static function Keywords($queryArray, $keyword){

    return $queryArray->where(function ($query) use($keyword) {
      $query->where("transfers.referencia", 'LIKE', "%{$keyword}%");
      $query->orWhere('transfers.banco', 'LIKE', "%{$keyword}%");
    });

  }

  public function paginate(Request $request){

      $pquery = DB::table('transfers')->where('trash', 0);

      if ($request->input('estado')) {
        $pquery->where('estado','=',$request->input('estado'));
      }

      if ($request->input('banco')) {
        $pquery->where('banco','=',$request->input('banco'));
      }

      if ($request->input('cuenta')) {
        $pquery->where('cuenta','=',$request->input('cuenta'));
      }

      if ($request->input('search')) {
         $pquery = TransferController::Keywords($pquery, $request->input('search'));
      }

      if ($request->input('by_dateStart')) {
        if($request->input("by_dateEnd")){
          $dates=[
            "0"=>Carbon::create($request->input('by_dateStart'))->toDateTimeString(),
            "1"=>Carbon::create($request->input("by_dateEnd"))->toDateTimeString(),
          ];
          $pquery=TransferController::DateRange($pquery,$dates,"created_at","transfers");
        }
        else {
          $pquery=TransferController::DateRange($pquery,Carbon::create($request->input('by_dateStart'))->toDateTimeString(),"created_at","volunteers");
        }
      }

      $transfers = [];

      //try {
          $transfers = MPage::paginate($pquery, $request, 30, '', 'transfers');
      /*} catch(\Exception $e) {
        var_dump($e);exit();
          return response()->json('Error al obtener datos', 500);
      }*/

      return $transfers;
  }

  public function chartData($startdate = null , $endate = null){
    if ($startdate && $endate){
      if (Carbon::parse($startdate) > Carbon::today()) return response()->json('La fecha inicial no puede ser mayor a la actual', 400);
      $start = Carbon::parse($startdate);
      $end = Carbon::parse($endate);
      $numero = DB::table('transfers')->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)->count();
      $pendientes = DB::table('transfers')->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)->where('estado','espera')->count();
      $aprobadas = DB::table('transfers')->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)->where('estado','aprobado')->count();
      $rechazadas = DB::table('transfers')->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)->where('estado','rechazado')->count();

      $capitalPendientes = DB::table('transfers')
      ->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)->where('estado','espera')->get()->toArray();
      $capitalSinConfirmar = 0;
      foreach ($capitalPendientes as $key) {
        $capitalSinConfirmar = $capitalSinConfirmar + $key->cantidad;
      }

      $capitalAprobadas = DB::table('transfers')
      ->where('trash',0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)
      ->where('estado','aprobado')->get()->toArray();
      $capitalAprobado = 0;
      foreach ($capitalAprobadas as $key) {
        $capitalAprobado = $capitalAprobado + $key->cantidad;
      }

      $capitalRechazadas = DB::table('transfers')
      ->where('trash',0)
      ->where('created_at','>=',$start)
      ->where('created_at','<=',$end)
      ->where('estado','rechazado')
      ->get()->toArray();
      $capitalRechazado = 0;
      foreach ($capitalRechazadas as $key) {
        $capitalRechazado = $capitalAprobado + $key->cantidad;
      }

      $bancosData = DB::table('transfers')
      ->where('trash', 0)
      ->where('created_at','>=',$start)->where('created_at','<=',$end)
      ->select('banco', DB::raw('SUM(cantidad) as total'))
      ->groupBy('banco')
      ->orderBy('total', 'desc')
      ->get();

      $bancos = array_map(function($data){
        return $data->banco;
      },$bancosData->toArray());

      $bancosCapital = array_map(function($data){
        return (int)$data->total;
      },$bancosData->toArray());

    }else {
      $numero = DB::table('transfers')->where('trash',0)->count();
      $pendientes = DB::table('transfers')->where('trash',0)->where('estado','espera')->count();
      $aprobadas = DB::table('transfers')->where('trash',0)->where('estado','aprobado')->count();
      $rechazadas = DB::table('transfers')->where('trash',0)->where('estado','rechazado')->count();

      $capitalPendientes = DB::table('transfers')
      ->where('trash',0)->where('estado','espera')->get()->toArray();
      $capitalSinConfirmar = 0;
      foreach ($capitalPendientes as $key) {
        $capitalSinConfirmar = $capitalSinConfirmar + $key->cantidad;
      }

      $capitalAprobadas = DB::table('transfers')
      ->where('trash',0)->where('estado','aprobado')->get()->toArray();
      $capitalAprobado = 0;
      foreach ($capitalAprobadas as $key) {
        $capitalAprobado = $capitalAprobado + $key->cantidad;
      }

      $capitalRechazadas = DB::table('transfers')
      ->where('trash',0)->where('estado','rechazado')->get()->toArray();
      $capitalRechazado = 0;
      foreach ($capitalRechazadas as $key) {
        $capitalRechazado = $capitalRechazado + $key->cantidad;
      }

      $bancosData = DB::table('transfers')
      ->where('trash', 0)
      ->where('estado','<>','rechazado')
      ->select('banco', DB::raw('SUM(cantidad) as total'))
      ->groupBy('banco')
      ->orderBy('total', 'desc')
      ->get();

      $bancos = array_map(function($data){
        return ucfirst($data->banco);
      },$bancosData->toArray());

      $bancosCapital = array_map(function($data){
        return (int)$data->total;
      },$bancosData->toArray());

    }
    $FI = date('Y-m'.'-01');
    $FI = date('Y-m-d', strtotime($FI.' - 1 day'));
    $FF = date('Y-m-t');
    $dateData = array();
    $dateDays = array();
    while ($FI <> $FF) {
      $FI = date('Y-m-d', strtotime($FI.' + 1 day'));
      $transfers = DB::table('transfers')->where('trash', 0)->where('estado','aprobado')->whereDate('updated_at', $FI)->get();
      $capital = 0;
      foreach ($transfers->toArray() as $key => $value) {
        $capital = $capital + $value->cantidad;
      }
      array_push($dateData,$capital);
      array_push($dateDays,(int)date('d', strtotime($FI)));
    }
    $data = [
      'transferencias' => $numero,
      'transferencias_rechazadas' => $rechazadas,
      'transferencias_pendientes' => $pendientes,
      'transferencias_aprobadas' => $aprobadas,
      'capital_rechazado' => $capitalRechazado,
      'capital_pendiente' => $capitalSinConfirmar,
      'capital_aprobado' => $capitalAprobado,
      'capital_total' => $capitalAprobado+$capitalSinConfirmar,
      'capitalBancoChart' => [['data' =>[$aprobadas,$pendientes,$rechazadas] ,'name' => 'Transferencias']],
      'labels' => $bancos,
      'series' => $bancosCapital,
      'dateData' => [['data' =>$dateData ,'name' => 'Capital Donado']],
      'dateDays' => ['xaxis' => ['categories' => $dateDays]],
    ];
    return response()->json($data);

  }

}
