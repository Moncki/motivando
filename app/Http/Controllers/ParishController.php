<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Parish;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Helpers\MPage;

class ParishController extends Controller
{
    protected function createParish(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|unique:parishes|min:4|max:64'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $parish = Parish::createParish($_request);
        if(!$parish) return response()->json("Error de servidor",500);

        return response()->json('Parroquia Creada Exitosamente',200);
    }

    protected function updateParish($id, Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'string|min:4|max:64'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $parish = Parish::find($id);
        if(!$parish) return response()->json("Parroquia no encontrada",404);
        $parish->nombre = $_request['nombre'];
        $parish->save();
        return response()->json('Parroquia Editada Exitosamente',200);
    }

    public function removeParish($id){

      $remove = Parish::removeParish($id);
      return $remove;

    }

    public function getAll(){
      $pquery = DB::table('parishes')->where('trash', 0);
      if (!$pquery) return response()->json('Error del servidor',500);

      if(!empty($pquery)){
        $data = array_map(function($parish){
          return [
            "value"=>$parish->id,
            "label"=>$parish->nombre,
          ];
        },$pquery->get()->toArray());
      }else{
        return [];
      }
      return $data;
    }

    public function paginate(Request $request){

        $pquery = DB::table('parishes')->where('trash', 0);

        $parishes = null;

        try {
            $parishes = MPage::paginate($pquery, $request, 20, '', 'parishes');
        } catch(\Exception $e) {
            return response()->json('Error al obtener datos', 500);
        }

        return $parishes;
    }

}
