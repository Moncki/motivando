<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Wallet;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\PaypalController;

class WalletController extends Controller
{
    /*protected function createWallet(Request $request){
        $validator = Validator::make($request->all(), [
            'direccion' => 'required|string|unique:wallets|min:4|max:64',
            'wallet' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $wallet = Wallet::createWallet($_request);
        if(!$wallet) return response()->json("Error de servidor",500);

        return response()->json('Wallet Creada Exitosamente',200);
    }*/

    protected function updateWallet($id, Request $request){
        $validator = Validator::make($request->all(), [
          'direccion' => 'required|string|min:4|max:64',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $wallet = Wallet::find($id);

        if(!$wallet) return response()->json("Wallet no encontrada",404);
        $wallet->direccion = $_request['direccion'];
        $wallet->save();
        return response()->json('Wallet Editada Exitosamente',200);
    }

    /*public function removeWallet($id){
      $remove = Wallet::removeWallet($id);
      return $remove;
    }*/

    public function getAll(){
      $pquery = DB::table('wallets')->where('trash', 0);
      if (!$pquery) return response()->json('Error del servidor',500);

      return $pquery->get();
    }

    public function balance(){
      $wallets = DB::table('wallets')->where('trash', 0)->get();

      $btc = file_get_contents('https://api.blockcypher.com/v1/btc/main/addrs/'.$wallets[0]->direccion.'/balance');

      sleep(10);

      $dash = file_get_contents('https://api.blockcypher.com/v1/dash/main/addrs/'.$wallets[1]->direccion.'/balance');

      $paypal = PaypalController::getBalance();
      
      sleep(2);

      $eth = file_get_contents('https://api.blockcypher.com/v1/eth/main/addrs/'.$wallets[2]->direccion.'/balance');

      $data = [
        json_decode($btc)->balance,
        json_decode($dash)->balance,
        json_decode($eth)->balance,
        $paypal
      ];
      return response()->json($data);
    }

}
