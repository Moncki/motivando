<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class googleAnalytics extends Controller
{

    public function getBy($metric, $dimension = null, $startdate = null , $endate = null){
      if (!$metric) return response()->json('Debe especificar una metrica', 400);
      $period = Period::days(7);
      if ($startdate){
        $start = Carbon::parse($startdate);
        $period = Period::create(Carbon::parse($startdate), Carbon::today());
      }
      if ($startdate && $endate){
        $period = Period::create(Carbon::parse($startdate), Carbon::parse($endate));
      }
      if ($dimension){
        $analyticsData = Analytics::performQuery($period, $metric ,['dimensions' => $dimension]);
      }else{
        $analyticsData = Analytics::performQuery($period, $metric);
      }
      $result = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);
      return response()->json($result);
    }

    public function visitorChart($startdate = null , $endate = null){
      $crecimiento = 0;
      if ($startdate && !$endate){
        if (Carbon::parse($startdate) > Carbon::today()) return response()->json('La fecha inicial no puede ser mayor a la actual', 400);
        $period = Period::create(Carbon::parse($startdate), Carbon::today());
        $analyticsData = Analytics::performQuery($period, 'ga:visits' ,['dimensions' => 'ga:userType']);
        $type = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:avgSessionDuration');
        $avgSessionDuration = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:pageviewsPerSession');
        $viewsPerSession = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:pageviews');
        $pageviews = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:bounceRate');
        $bounceRate = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:visits');
        $visits = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:visits',['dimensions' => 'ga:day']);
        $visitsChart = googleAnalytics::formatResponseChart($analyticsData->rows, $analyticsData->columnHeaders, 'Visitantes');

        $analyticsData = Analytics::performQuery($period, 'ga:visitors');
        $visitors = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);
        $data = [
          'tipo' => [
             (isset($type[1][1]) && !is_null($type[1][1])) ? (int) $type[1][1] : 0,
             (isset($type[2][1]) && !is_null($type[2][1])) ? (int) $type[2][1] : 0
          ],
          'visitas' => (int)$visits[1][0],
          'visitasData' => $visitsChart,
          'visitantes' => (int)$visitors[1][0],
          'duracion' => (int)$avgSessionDuration[1][0],
          'crecimiento' => $crecimiento,
          'rebote' => (float)$bounceRate,
          'vistassesion' => (float)$viewsPerSession[1][0],
          'paginasVistas' => $pageviews[1][0]
        ];
        return response()->json($data);
      }
      if ($startdate && $endate){
        if (Carbon::parse($startdate) > Carbon::today()) return response()->json('La fecha inicial no puede ser mayor a la actual', 400);
        $period = Period::create(Carbon::parse($startdate), Carbon::parse($endate));

        $analyticsData = Analytics::performQuery($period, 'ga:visits' ,['dimensions' => 'ga:userType']);

        $type = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);
        $analyticsData = Analytics::performQuery($period, 'ga:avgSessionDuration');
        $avgSessionDuration = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:pageviews');
        $pageviews = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:pageviewsPerSession');
        $viewsPerSession = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:bounceRate');
        $bounceRate = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:visitors');
        $visitors = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:visits');
        $visitsBefore = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $analyticsData = Analytics::performQuery($period, 'ga:visits',['dimensions' => 'ga:day']);
        $visitsChart = googleAnalytics::formatResponseChart($analyticsData->rows, $analyticsData->columnHeaders, 'Visitantes');

        $period = Period::create(Carbon::parse($endate), Carbon::today());
        $analyticsData = Analytics::performQuery($period, 'ga:visits');
        $visitsAfter = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

        $visitsBefore = (int)$visitsBefore[1][0];
        $visitsAfter = (int)$visitsAfter[1][0];
        if ($visitsBefore > 0)
        $crecimiento = (($visitsBefore - $visitsAfter)/$visitsAfter) * 100;
        $visits = $visitsBefore;



        $data = [
          'tipo' => [
             (isset($type[1][1]) && !is_null($type[1][1])) ? (int) $type[1][1] : 0,
             (isset($type[2][1]) && !is_null($type[2][1])) ? (int) $type[2][1] : 0
          ],
          'visitas' => $visits,
          'visitasData' => $visitsChart,
          'visitantes' => (int)$visitors[1][0],
          'duracion' => (int)$avgSessionDuration[1][0],
          'crecimiento' => $crecimiento,
          'rebote' => (float)$bounceRate,
          'vistassesion' => (float)$viewsPerSession[1][0],
          'paginasVistas' => $pageviews[1][0]
        ];
        return response()->json($data);
      }

      $period = Period::days(((integer)date('d'))-1);
      $analyticsData = Analytics::performQuery($period, 'ga:visits' ,['dimensions' => 'ga:userType']);
      $type = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:avgSessionDuration');
      $avgSessionDuration = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:pageviews');
      $pageviews = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:pageviewsPerSession');
      $viewsPerSession = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:bounceRate');
      $bounceRate = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:visitors');
      $visitors = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:visits');
      $visits = googleAnalytics::formatResponse($analyticsData->rows, $analyticsData->columnHeaders);

      $analyticsData = Analytics::performQuery($period, 'ga:visits',['dimensions' => 'ga:day']);
      $visitsChart = googleAnalytics::formatResponseChart($analyticsData->rows, $analyticsData->columnHeaders, 'Visitantes');

      $data = [
        'tipo' => [
          (int)$type[1][1],
          (int)$type[2][1]
        ],
        'visitas' => (int)$visits[1][0],
        'visitasData' => $visitsChart,
        'visitantes' => (int)$visitors[1][0],
        'duracion' => (int)$avgSessionDuration[1][0],
        'crecimiento' => $crecimiento,
        'rebote' => (float)$bounceRate[1][0],
        'vistassesion' => (float)$viewsPerSession[1][0],
        'paginasVistas' => $pageviews[1][0],
      ];
      return response()->json($data);
    }

    public function formatResponse($rows, $headers){
      $finalData = array();
      $columnHeaders = array_map(function($data){
        return $data->name;
      },$headers);
      array_push($finalData,$columnHeaders);
      if (!$rows) {
        $rowsdata = [[[]],[[]]];
      }
      else{
        $rowsdata = array_map(function($data){
          return $data;
        },$rows);
      }
      $result = array_merge($finalData, $rowsdata);
      return $result;
    }

    public function formatResponseChart($rows, $headers, $name = 'items'){
      $rowsDay = array_map(function($data){
        return (int)$data[0];
      },$rows);
      if (!$rows) {
        $rowsdata = [[[]],[[]]];
      }
      else{
        $rowsData = array_map(function($data){
          return (int)$data[1];
        },$rows);
      }
      $result = [
        'xaxis' => ['categories' => $rowsDay],
        'rows' => [['data' =>$rowsData, 'name' => $name]],
      ];
      return $result;
    }


}
