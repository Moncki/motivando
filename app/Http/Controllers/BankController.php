<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Bank;

class BankController extends Controller
{
    protected function createBank(Request $request){

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|min:2|max:48',
            'titular' => 'required|string|min:2|max:48',
            'identificacion' => 'required|string|min:6|max:10',
            'cuenta' => 'required|string|min:16|max:20',
            'banco' => 'required|in:banesco,venezuela,provincial',
            'email' => 'string|min:4|max:180',
            'telefono' => 'string|min:10|max:20',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $bank = Bank::createBank($_request);
        if(!$bank) return response()->json("Error de servidor",500);

        return response()->json('Cuenta bancaria creada',200);

    }

    protected function editBank($id,Request $request){

      $validator = Validator::make($request->all(), [
          'nombre' => 'string|min:2|max:48',
          'titular' => 'string|min:2|max:48',
          'identificacion' => 'string|min:6|max:10',
          'cuenta' => 'string|min:16|max:20',
          'email' => 'string|min:4|max:180',
          'telefono' => 'string|min:10|max:14',
      ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $_request = $request->all();

        $bank = Bank::editBank($id,$_request);
        if (!$bank) return response()->json('Error de servidor',200);

        return response()->json('Cuenta bancaria editada',200);
    }

    public function removeBank($id){

      $remove = Bank::removeBank($id);
      return $remove;

    }

    public function getAll(){
      $pquery = DB::table('banks')->where('trash', 0);
      if (!$pquery) return response()->json('Error del servidor',500);

      return $pquery->get();
    }
}
