<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Naif\LaravelPayPal\LaravelPayPal;
class PaypalController extends Controller
{
    public static function getBalance(){
      $paypal = new LaravelPayPal();
      $balance = $paypal->getBalance();
      $balance = $balance['balance']['L_AMT0'];
      $balance = (float)$balance;
      return $balance;
      //return response()->json($balance);
    }

    public function getcosas(){
      $paypal = new LaravelPayPal();
      $transactions = $paypal->getTransactions();
      return response()->json($transactions);
    }

}
