<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Volunteer;
use App\Helpers\MPage;
use Carbon\Carbon;

class VolunteerController extends Controller
{
    protected function createVolunteer(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|min:2|max:32',
            'apellido' => 'required|string|min:2|max:32',
            'cedula' => 'required|string|min:6|max:8',
            'email' => 'required|unique:volunteers|string|min:4|max:180',
            'telefono' => 'required|string|min:10|max:16',
            'nacimiento' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $_request = $request->all();

        $volunteer = Volunteer::createVolunteer($_request);
        if(!$volunteer) return response()->json("Error de servidor",500);

        return response()->json('Bienvenido a bordo',200);
    }

    public function removeVolunteer($id){

      $remove = Volunteer::removeVolunteer($id);
      return $remove;

    }

    public static function DateRange($query,$date,$param,$tableName){
      if(is_array($date)){
        $startDate=$date[0];
        $endDate=$date[1];

        return $query->whereBetween($tableName.'.'.$param,[$startDate,$endDate]);
        //where($tableName.'.'.$param, '>=', $startDate)
        //->where($tableName.'.'.$param, '<=', $endDate);
      }

      return $query->where($tableName.'.'.$param,'=',$date);
    }

    public static function Keywords($queryArray, $keyword){

      return $queryArray->where(function ($query) use($keyword) {
        $query->where("volunteers.nombre", 'LIKE', "%{$keyword}%");
        $query->orWhere('volunteers.apellido', 'LIKE', "%{$keyword}%");
        $query->orWhere('volunteers.cedula', 'LIKE', "%{$keyword}%");
        $query->orWhere('parishes.nombre', 'LIKE', "%{$keyword}%");
      });

    }

    public function paginate(Request $request){

        $pquery = DB::table('volunteers')
        ->where('volunteers.trash', 0)
        ->join('parishes', 'parishes.id', '=', 'volunteers.parroquia')
        ->selectRaw('volunteers.*, parishes.nombre as parroquia');

        if($request->input("by_keyword")){
          $pquery= VolunteerController::Keywords($pquery, $request->input("by_keyword"));
        }

        if($request->input("by_parish")){
          $pquery->where("volunteers.parroquia", "=" , $request->input("by_parish"));
        }

        if ($request->input('by_dateStart')) {
          if($request->input("by_dateEnd")){
            $dates=[
              "0"=>Carbon::create($request->input('by_dateStart'))->toDateTimeString(),
              "1"=>Carbon::create($request->input("by_dateEnd"))->toDateTimeString(),
            ];
            $pquery=VolunteerController::DateRange($pquery,$dates,"created_at","volunteers");
          }
          else {
            $pquery=VolunteerController::DateRange($pquery,Carbon::create($request->input('by_dateStart'))->toDateTimeString(),"created_at","volunteers");
          }
        }

        $volunteers = null;

        try {
            $volunteers = MPage::paginate($pquery, $request, 30, '', 'volunteers');
        } catch(\Exception $e) {
            return response()->json('Error al obtener datos', 500);
        }

        return $volunteers;
    }

    public function chartData($startdate = null , $endate = null){
      $FI = date('Y-m'.'-01');
      $FI = date('Y-m-d', strtotime($FI.' - 1 day'));
      $FF = date('Y-m-t');
      $crecimiento = 0;
      if ($startdate && $endate){
        if (Carbon::parse($startdate) > Carbon::today()) return response()->json('La fecha inicial no puede ser mayor a la actual', 400);
        $start = Carbon::parse($startdate);
        $end = Carbon::parse($endate);
        $totalantes= DB::table('volunteers')->where('trash', 0)
        ->where('created_at','<',$start)->count();
        $totaldespues = DB::table('volunteers')->where('trash', 0)
        ->where('created_at','>=',$start)->count();
        if ($totalantes != 0){
          $crecimiento = (($totaldespues - $totalantes)/ $totalantes) * 100;
        }
        $total = DB::table('volunteers')->where('trash', 0)
        ->where('created_at','>=',$start)->where('created_at','<=',$end)->count();

        $parroquias = DB::table('volunteers')
        ->where('trash', 0)
        ->where('created_at','>=',$start)->where('created_at','<=',$end)
        ->select('parroquia', DB::raw('count(*) as total'))
        ->groupBy('parroquia')
        ->orderBy('total', 'desc')
        ->get();
        $parroquia = array_map(function($data){
          $db = DB::table('parishes')->where('trash', 0)->where('id',$data->parroquia)->first();
          return $db->nombre;
        },$parroquias->toArray());
        $totalxParroquia = array_map(function($data){
          return $data->total;
        },$parroquias->toArray());
        $totalParroquias = 0;
        foreach ($totalxParroquia as $key) {
          $totalParroquias = $totalParroquias + $key;
        }

        $masculino = DB::table('volunteers')
        ->where('trash', 0)
        ->where('created_at','>=',$start)->where('created_at','<=',$end)
        ->where('genero','masculino')
        ->count();
        $femenino= DB::table('volunteers')
        ->where('trash', 0)
        ->where('created_at','>=',$start)->where('created_at','<=',$end)
        ->where('genero','femenino')
        ->count();

        $dateData = array();
        $dateDays = array();
        $FI = date('Y-m-d', strtotime($start.' - 1 day'));
        $FF = date('Y-m-d', strtotime($end));
        while ($FI <> $FF) {
          $FI = date('Y-m-d', strtotime($FI.' + 1 day'));
          $volunteers = DB::table('volunteers')->where('trash', 0)->whereDate('created_at', $FI)->count();
          array_push($dateData,$volunteers);
          array_push($dateDays,(int)date('d', strtotime($FI)));
        }

      }else{
        $total = DB::table('volunteers')->where('trash', 0)->count();

        $masculino = DB::table('volunteers')->where('trash', 0)->where('genero','masculino')->count();
        $femenino= DB::table('volunteers')->where('trash', 0)->where('genero','femenino')->count();

        $parroquias = DB::table('volunteers')
        ->where('trash', 0)
        ->select('parroquia', DB::raw('count(*) as total'))
        ->groupBy('parroquia')
        ->orderBy('total', 'desc')
        ->get();
        $parroquia = array_map(function($data){
          $db = DB::table('parishes')->where('trash', 0)->where('id',$data->parroquia)->first();
          return $db->nombre;
        },$parroquias->toArray());
        $totalxParroquia = array_map(function($data){
          return $data->total;
        },$parroquias->toArray());
        $totalParroquias = 0;
        foreach ($totalxParroquia as $key) {
          $totalParroquias = $totalParroquias + $key;
        }

        $dateData = array();
        $dateDays = array();
        while ($FI <> $FF) {
          $FI = date('Y-m-d', strtotime($FI.' + 1 day'));
          $volunteers = DB::table('volunteers')->where('trash', 0)->whereDate('created_at', $FI)->count();
          array_push($dateData,$volunteers);
          array_push($dateDays,(int)date('d', strtotime($FI)));
        }
      }

      $data = [
        'voluntarios' => $total,
        'dateData' => [['data' =>$dateData ,'name' => 'Voluntarios registrados']],
        'dateDays' => ['xaxis' => ['categories' => $dateDays]],
        'genero' => [
          $masculino,
          $femenino
        ],
        'parroquias' => [
          'labels' => $parroquia,
          'totalxParroquia' => $totalxParroquia,
          'totalParroquias' => $totalParroquias
        ],
        'crecimiento' => $crecimiento
      ];

      return response()->json($data);
    }

}
