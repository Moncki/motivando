<?php namespace App\Helpers;

  use Mail;

  class MailHelper {

    public static function sendMail($user,$title="",$desc="",$files=[]){
      //var_dump($desc, $title); exit();
        Mail::send('email.message', ['data' => $user, "title"=>$title, "description"=>$desc], function ($m)
        use ($user,$files,$title) {
            foreach($files as $file){
              $m->attach($file["path"], array(
                'as' => $file["name"],
                'mime' => $file["mime"])
              );
            }//verify_token
            $m->to('web.motivandovenezuela@gmail.com', $user['name'])->subject($title);
         });

    }

  }

?>
