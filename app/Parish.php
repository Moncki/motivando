<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Parish extends Model{

    protected $fillable = [
        'id',
        'nombre',
        'trash'
    ];

    public static function createParish($request){

        return Parish::create([
            "nombre" => $request["nombre"],
        ]);
    }

    public static function removeParish($id){
      $parish = Parish::find($id);
      if (!$parish) return response()->json('Parroquia no encontrado', 404);

      $parish->trash = 1;
      $parish->save();

      return response()->json('Parroquia Eliminada', 200);
    }

}
