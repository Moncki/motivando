<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wallet extends Model{
    protected $fillable = [
        'id',
        'wallet',
        'direccion',
        'trash'
    ];

    /*public static function createWallet($request){
        return Wallet::create([
            "wallet" => $request["wallet"],
            "direccion" => $request["direccion"],
        ]);
    }*/

    public static function removeWallet($id){
      $wallet = Wallet::find($id);
      if (!$wallet) return response()->json('Wallet no encontrada', 404);

      $wallet->trash = 1;
      $wallet->save();

      return response()->json('Wallet Eliminada', 404);
    }
}
