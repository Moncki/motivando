<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class Transfer extends Model{

  protected $fillable = [
      'id',
      'referencia',
      'captura',
      'banco',
      'cantidad',
      'cuenta',
      'trash'
  ];

  public static function createTransfer($request){

    $keysAllow = [
      'referencia',
      'captura',
      'banco',
      'cantidad',
      'cuenta'
    ];

    $itemToSave = [];

    foreach ($keysAllow as $key){
      if (isset($request[$key])) $itemToSave[$key] = $request[$key];
      else $itemToSave[$key] = null;
    }

    if (isset($request['captura'])){
      $file = $request['captura'];
      $carpetaDestino = 'uploads';
      $nombreArchivo = 'file_'.uniqid().'_'.$file->getClientOriginalName();
      $urlPhoto = $file->move($carpetaDestino,$nombreArchivo);
      $itemToSave['captura'] = $urlPhoto;
    }

    return Transfer::create($itemToSave);

  }

  public static function removeTransfer($id){
    $transfer= Transfer::find($id);
    if (!$transfer) return response()->json('Transferencia no encontrada', 404);

    $transfer->trash = 1;
    $transfer->save();

    return response()->json('Transferencia eliminada', 404);
  }

}
