<?php

namespace App\Exports;

use App\Volunteer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class VolunteersExport implements FromView
{
    public function view(): View
    {
        $volunteer = Volunteer::all();
        $volunteer = $volunteer->toArray();
        $volunteers = array_map(function($data){
          if ($data['parroquia']){
            $parroquia = DB::table('parishes')->where('trash', 0)->where('id',$data['parroquia'])->first();
            $parroquia = $parroquia->nombre;
          }
          return [
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'cedula' => $data['cedula'],
            'genero' => $data['genero'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'nacimiento' => $data['nacimiento'],
            'parroquia' => $parroquia,
            'creacion' => $data['created_at'],
          ];
        },$volunteer);
        return view('excel', [
            'volunteers' => $volunteers
        ]);
    }
}
