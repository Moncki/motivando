<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Volunteer extends Model
{
    protected $fillable = [
        'id',
        'nombre',
        'apellido',
        'cedula',
        'genero',
        'email',
        'telefono',
        'nacimiento',
        'parroquia',
        'trash'
    ];

    public static function createVolunteer($request){
        $keysAllow = [
          'nombre',
          'apellido',
          'cedula',
          'genero',
          'email',
          'telefono',
          'nacimiento',
          'parroquia'
        ];

        $itemToSave = [];

        foreach ($keysAllow as $key){
          if (isset($request[$key])) $itemToSave[$key] = $request[$key];
          else $itemToSave[$key] = null;
        }

        return Volunteer::create($itemToSave);

    }

    public static function removeVolunteer($id){
      $volunteer = Volunteer::find($id);
      if (!$volunteer) return response()->json('Voluntario no encontrado', 404);

      $volunteer->trash = 1;
      $volunteer->save();

      return response()->json('Voluntario Eliminado', 404);
    }

}
