<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'id',
        'username',
        'password',
        'email',
        'name',
        'last_name',
        'phone',
        'avatar',
        'role',
        'slug',
        'trash'
    ];

    protected $hidden = ['password'];

    public static function slugify($string){
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }

    public static function createUser($request){

        $fullName = '';
        $fullName = (isset($request['name'])) ? $request['name'] : '';

        if (isset($request['last_name']))
          $fullName .= ' '. $request['last_name'];

        $slugOrigin = User::slugify($fullName);
        $slug = $slugOrigin;
        $i = 0;
        while (User::where('slug','=',$slug)->first()) {
          $i++;
          $slug = $slugOrigin.'-'.$i;
        }

        return User::create([
            "username" => $request["username"],
            "password" => Hash::make($request['password']),
            "email" => $request["email"],
            "name" => (isset($request['name'])) ? $request['name'] : null,
            "last_name" => (isset($request['last_name'])) ? $request['last_name'] : null,
            "phone" => (isset($request['phone'])) ? $request['phone'] : null,
            "role" => (isset($request['role'])) ? $request['role'] : 1,
            'slug' => $slug,
            'avatar' => (isset($request['avatar'])) ? $request['avatar'] : null
        ]);

    }

    public static function editUser($request){

      if (isset($request['user_id'])){
        $user = User::find($request['user_id']);
      }else{
        $user = Auth::user();
        $user = User::find($user->id);
      }
      if (!$user) return response()->json('Usuario no encontrado', 404);

      if (isset($request['avatar'])){
        $file = $request['avatar'];
        $carpetaDestino = 'uploads';
        $nombreArchivo = 'file_'.uniqid().'_'.$file->getClientOriginalName();
        $urlPhoto = $file->move($carpetaDestino,$nombreArchivo);
        $user->avatar = $urlPhoto;
      }

      if (isset($request['email'])) $user->email = $request['email'];
      if (isset($request['password'])) $user->password = Hash::make($request['password']);
      if (isset($request['name'])) $user->name = $request['name'];
      if (isset($request['last_name'])) $user->last_name = $request['last_name'];
      if (isset($request['phone'])) $user->phone = $request['phone'];
      if (isset($request['role'])) $user->role = $request['role'];
      $user->save();
    }

    public static function getUser($slug){
      $query = DB::table('users')->where('slug',$slug);
      return $query->get();
    }

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

}
