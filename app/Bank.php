<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Bank extends Model{
    protected $fillable = [
        'id',
        'nombre',
        'titular',
        'identificacion',
        'cuenta',
        'banco',
        'tipo',
        'email',
        'foto',
        'pagomovil',
        'telefono',
        'personal',
        'trash'
    ];

    public static function createBank($request){
        $keysAllow = [
          'nombre',
          'titular',
          'identificacion',
          'cuenta',
          'banco',
          'tipo',
          'email',
          'pagomovil',
          'personal',
          'telefono'
        ];

        $itemToSave = [];

        foreach ($keysAllow as $key){
          if (isset($request[$key])) $itemToSave[$key] = $request[$key];
          else $itemToSave[$key] = null;
        }

        if (isset($request['foto'])){
          $file = $request['foto'];
          $carpetaDestino = 'uploads';
          $nombreArchivo = 'file_'.uniqid().'_'.$file->getClientOriginalName();
          $urlPhoto = $file->move($carpetaDestino,$nombreArchivo);
          $itemToSave['foto'] = $urlPhoto;
        }

        return Bank::create($itemToSave);

    }

    public static function editBank($id, $request){

      $bank= Bank::find($id);
      if (!$bank) return response()->json('Cuenta bancaria no encontrada', 404);

      $keysAllow = [
        'nombre',
        'identificacion',
        'cuenta',
        'banco',
        'tipo',
        'email',
        'foto',
        'pagomovil',
        'personal',
        'telefono'
      ];


      foreach ($keysAllow as $key)
          if (isset($request[$key]))
              $bank->{$key} = $request[$key];

      if (isset($request['foto'])){
        $file = $request['foto'];
        $carpetaDestino = 'uploads';
        $nombreArchivo = 'file_'.uniqid().'_'.$file->getClientOriginalName();
        $urlPhoto = $file->move($carpetaDestino,$nombreArchivo);
        $bank->foto = $urlPhoto;
      }

      if(!$bank->save()) return false;
      return true;
    }

    public static function removeBank($id){
      $bank= Bank::find($id);
      if (!$bank) return response()->json('Cuenta bancaria no encontrada', 404);

      $bank->trash = 1;
      $bank->save();

      return response()->json('Cuenta bancaria eliminada', 404);
    }

}
