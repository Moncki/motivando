<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',32)->unique();
            $table->string('password',180);
            $table->string("email",180)->nullable()->unique();
            $table->string("phone", 16)->nullable();
            $table->string("name", 32)->nullable();
            $table->string("last_name", 32)->nullable();
            $table->string('avatar')->nullable();
            $table->enum("role",["usuario","admin"])->default("usuario");
            $table->string("slug", 70)->unique();
            $table->boolean("trash")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
