<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string("referencia");
            $table->string("captura")->nullable();
            $table->enum("banco",[
              "banesco",
              "venezuela",
              "provincial"
            ]);

            /*Si se añade aca se debe añadir en el
              in:banesco,venezuela,provincial
            de TransferController.php*/

            $table->enum("estado",[
              "espera",
              "aprobado",
              "rechazado"
            ])->default("espera");
            $table->decimal("cantidad",24,2);
            $table->boolean("trash")->default(0);
            $table->bigInteger('cuenta')->unsigned()->nullable();
            $table->foreign('cuenta')->references('id')->on('banks');
            //$table->boolean("trash")->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
