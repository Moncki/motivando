<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nombre", 48);
            $table->string("titular", 48);
            $table->string('identificacion',9);
            $table->string("cuenta", 24);
            $table->enum("banco",[//se deben agregar al in:banesco,venezuela,provincial en BankController
              "banesco",
              "venezuela",
              "provincial"
            ]);
            $table->enum("tipo",["corriente","ahorro","maxima"])->nullable();
            $table->string("email",180)->nullable();
            $table->string("foto")->nullable();
            $table->boolean("pagomovil")->default(0);
            $table->string("telefono", 14)->nullable();
            $table->boolean("personal")->default(0);
            $table->boolean("trash")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
