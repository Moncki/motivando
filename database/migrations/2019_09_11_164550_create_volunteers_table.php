<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("nombre", 32)->nullable();
            $table->string("apellido", 32)->nullable();
            $table->string('cedula',8)->nullable()->unique();
            $table->enum("genero",["masculino","femenino"])->nullable();
            $table->string("email",180)->unique()->nullable();
            $table->string("telefono", 16)->nullable();
            $table->date("nacimiento")->nullable();

            $table->bigInteger('parroquia')->unsigned()->nullable();
            $table->foreign('parroquia')->references('id')->on('parishes');

            $table->boolean("trash")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
