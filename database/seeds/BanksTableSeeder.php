<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("banks")->insert([
          "nombre"=> 'Cuenta Mercantil Ahorro',
          "titular"=> 'Jhon Doe',
          "identificacion"=> '27938288',
          "cuenta"=> '05793510478264937185',
          "banco"=> 'banesco',
          "tipo"=> 'ahorro',
          "email"=> 'jhon_doe@gmail.com',
          "tipo"=> 'ahorro',
        ]);

        DB::table("banks")->insert([
          "nombre"=> 'Cuenta del Venezuela Corriente',
          "titular"=> 'Jenny Doe',
          "identificacion"=> '272012276',
          "cuenta"=> '5050105015050105501',
          "banco"=> 'venezuela',
          "tipo"=> 'ahorro',
          "email"=> 'jenny_doe@gmail.com',
          "tipo"=> 'corriente',
          "pagomovil"=> 1,
          "telefono"=> '4249050162',
          "personal"=> 1,
        ]);

    }
}
