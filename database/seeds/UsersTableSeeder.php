<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
          "username"=> 'admin',
          "password"=> Hash::make('admin'),
          "email"=> "admin@admin.com",
          "name"=> 'admin',
          "last_name"=> 'admin',
          "slug"=> "admin-admin",
          "role"=> "admin",
        ]);
        DB::table("users")->insert([
          "username"=> 'Moncki',
          "password"=> Hash::make('27935371'),
          "email"=> "moncki21@gmail.com",
          "name"=> 'Eduardo',
          "last_name"=> 'Lara',
          "phone"=> '04249050162',
          "slug"=> "eduardo-lara",
          "role"=> "admin",
        ]);
        DB::table("users")->insert([
          "username"=> 'darens',
          "password"=> Hash::make('1234'),
          "email"=> "darens@gmail.com",
          "name"=> 'Andres',
          "last_name"=> 'Rodrigues',
          "phone"=> '04147721568',
          "slug"=> "andres-rodrigues",
          "role"=> "usuario",
        ]);
        DB::table("users")->insert([
          "username"=> 'hectorxd',
          "password"=> Hash::make('martha2'),
          "email"=> "HectorEFerrerC@hotmail.com",
          "name"=> 'Hector',
          "last_name"=> 'Ferrer',
          "phone"=> '04126998473',
          "slug"=> "hector-ferrer",
          "role"=> "usuario",
        ]);
    }
}
