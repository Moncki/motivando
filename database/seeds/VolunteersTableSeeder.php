<?php

use Illuminate\Database\Seeder;

class VolunteersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("volunteers")->insert([
          "nombre"=> 'Eduardo',
          "apellido"=> 'Lara',
          "cedula"=> '27935371',
          "genero"=> 'masculino',
          "email"=> 'eduardo@gmail.com',
          "telefono"=> '04249050162',
          "parroquia"=> 1,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
        DB::table("volunteers")->insert([
          "nombre"=> 'Hector',
          "apellido"=> 'Ferrer',
          "cedula"=> '27903371',
          "genero"=> 'masculino',
          "email"=> 'hector@gmail.com',
          "telefono"=> '04126992473',
          "parroquia"=> 1,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
        DB::table("volunteers")->insert([
          "nombre"=> 'Andres',
          "apellido"=> 'Rodrigues',
          "cedula"=> '27905471',
          "genero"=> 'masculino',
          "email"=> 'andres@gmail.com',
          "telefono"=> '04147721568',
          "parroquia"=> 1,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
        DB::table("volunteers")->insert([
          "nombre"=> 'Alejandro',
          "apellido"=> 'Zurita',
          "cedula"=> '28201905',
          "genero"=> 'masculino',
          "email"=> 'alejandro@gmail.com',
          "telefono"=> '04161995403',
          "parroquia"=> 2,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
        DB::table("volunteers")->insert([
          "nombre"=> 'Luis',
          "apellido"=> 'Bravo',
          "cedula"=> '29075201',
          "genero"=> 'masculino',
          "email"=> 'luis@gmail.com',
          "telefono"=> '04149654911',
          "parroquia"=> 2,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
        DB::table("volunteers")->insert([
          "nombre"=> 'Gabriel',
          "apellido"=> 'Marshandet',
          "cedula"=> '28540250',
          "genero"=> 'masculino',
          "email"=> 'gabriel@gmail.com',
          "telefono"=> '04249104569',
          "parroquia"=> 1,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
    }
}
