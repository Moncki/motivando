<?php

use Illuminate\Database\Seeder;

class TransfersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
      DB::table("transfers")->insert([
        "referencia"=> '5294345',
        "banco"=> 'banesco',
        "estado"=> 'espera',
        "cantidad"=> 225,
        'cuenta' => 1,
        'created_at' => date('Y-m-'.rand(1,20)),
      ]);
      DB::table("transfers")->insert([
        "referencia"=> '9294345',
        "banco"=> 'venezuela',
        "estado"=> 'aprobado',
        "cantidad"=> 100,
        'cuenta' => 1,
        'created_at' => date('Y-m-'.rand(1,20)),
      ]);
      DB::table("transfers")->insert([
        "referencia"=> '9294345',
        "banco"=> 'venezuela',
        "estado"=> 'espera',
        "cantidad"=> 500,
        'cuenta' => 2,
        'created_at' => date('Y-m-'.rand(1,20)),
      ]);
      DB::table("transfers")->insert([
        "referencia"=> '9294345',
        "banco"=> 'venezuela',
        "estado"=> 'rechazado',
        "cantidad"=> 380,
        'cuenta' => 1,
        'created_at' => date('Y-m-'.rand(1,20)),
      ]);
      for ($i=0; $i < 50; $i++) {
        DB::table("transfers")->insert([
          "referencia"=> '9294345',
          "banco"=> 'venezuela',
          "estado"=> 'espera',
          "cantidad"=> 250,
          'cuenta' => 2,
          'created_at' => date('Y-m-'.rand(1,20)),
        ]);
      }
    }
}
