<?php

use Illuminate\Database\Seeder;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ParishesTableSeeder::class);
        $this->call(VolunteersTableSeeder::class);
        $this->call(WalletsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(TransfersTableSeeder::class);
        factory(Product::class, 50)->create();
    }
}
