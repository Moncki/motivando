<?php

use Illuminate\Database\Seeder;

class ParishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("parishes")->insert([
          "nombre"=> 'Universidad',
        ]);
        DB::table("parishes")->insert([
          "nombre"=> 'Caroni',
        ]);
    }
}
