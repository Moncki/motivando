<?php

use Illuminate\Database\Seeder;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("wallets")->insert([
          "wallet"=> 'bitcoin',
          'direccion' => '15hZKTkr1n7PcT41TYcDhnhWTFKJsSLVor'
        ]);
        DB::table("wallets")->insert([
          "wallet"=> 'dash',
          'direccion' => 'XwhHitA5qSsv8QXkXTPUQNCN3FoxrgBHMT'
        ]);
        DB::table("wallets")->insert([
          "wallet"=> 'ethereum',
          'direccion' => '0xEBBd2750104d83E23bBe7ae26C81904e9e28cE7F'
        ]);
    }
}
