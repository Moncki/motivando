<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->title,
        'description' => $faker->paragraph(1),
        'serial' => mt_rand(0, 0xffff),
        'stock' => $faker->numberBetween(0, 200)
    ];
});
