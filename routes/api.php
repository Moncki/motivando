<?php

use Illuminate\Http\Request;
use App\Exports\VolunteersExport;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['JwtMiddleware']], function () {

  //Route::post('/user', 'UsersController@createUser');  //Crear Usuario
  //Route::put('/user/{id}', 'UsersController@editUser');  //Editar Usuario
  //Route::get("/users", "UsersController@paginate");  //Lista de usuarios
  //Route::get("/user/{slug}", "UsersController@getUser");  //Buscar Usuario

  Route::get('/me', 'UsersController@me');


});
Route::get("/volunteers", "VolunteerController@paginate");  //Lista de voluntarios
Route::get("/volunteers/chartData/{startdate?}/{endate?}", "VolunteerController@chartData");
Route::delete('/volunteer/{id}', 'VolunteerController@removeVolunteer');  //Eliminar Voluntario

Route::get    ("/transfers", "TransferController@paginate");         //Lista de transferencias
Route::delete ("/transfers", "TransferController@removeTransfer");   //Eliminar transferencias
Route::put    ('/transfers/{id}', 'TransferController@updateTransfer');   //Editar estado transferencias

Route::post('/parish', 'ParishController@createParish');  //Crear Parroquia
Route::put('/parish/{id}', 'ParishController@updateParish');  //Editar Parroquia
Route::delete('/parish/{id}', 'ParishController@removeParish');  //Eliminar Parroquia
Route::get("/parishes/pages", "ParishController@paginate");  //Lista de Parroquias para paginado

Route::post('/bank', 'BankController@createBank');  //Crear Cuenta bancaria
Route::put('/bank/{id}', 'BankController@editBank');  //Editar Cuenta bancaria
Route::delete('/bank/{id}', 'BankController@removeBank');  //Eliminar Cuenta bancaria
Route::get("/banks", "BankController@getAll");  //Lista de cuentas bancarias sin paginado

Route::post('/wallet', 'WalletController@createWallet');  //Crear Wallet
Route::put('/wallet/{id}', 'WalletController@updateWallet');  //Editar Wallet
Route::delete('/wallet/{id}', 'WalletController@removeWallet');  //Eliminar Wallet
Route::get("/wallets", "WalletController@getAll");  //Lista de wallets sin paginado
Route::get("/wallets/balance", "WalletController@balance");

Route::get("/transfers/chartData/{startdate?}/{endate?}", "TransferController@chartData");

Route::get("/paypal/balance", "PaypalController@getBalance");
Route::get("/paypal/getcosas", "PaypalController@getcosas");

// Publicas
Route::post('/volunteer', 'VolunteerController@createVolunteer');  //Crear Voluntario
Route::post('/transfers', 'TransferController@createTransfer');   //Crear transferencia
Route::get("/parishes", "ParishController@getAll");  //Lista de Parroquias, creo que para el modal de voluntarios
Route::post('/login', 'Auth\LoginController@enter');

// Google analitcs
Route::get('/analytics/{metric}/{dimension?}/{startdate?}/{endate?}', 'googleAnalytics@getBy');

Route::get('/visitorChart/{startdate?}/{endate?}', 'googleAnalytics@visitorChart');

Route::post('/contact', 'MailController@send');

Route::get('/excel', function () {
    return Excel::download(new VolunteersExport, 'voluntarios.xlsx');
});
