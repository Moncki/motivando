<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
  </head>
  <body>
    <table>
      <thead>
      <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Cedula</th>
          <th>Genero</th>
          <th>Email</th>
          <th>Telefono</th>
          <th>Fecha de Nacimiento</th>
          <th>Parroquia</th>
          <th>Fecha de Inscripcion</th>
      </tr>
      </thead>
      <tbody>
      @foreach ($volunteers as $volunteer)
          <tr>
              <td>{{ $volunteer['nombre'] }}</td>
              <td>{{ $volunteer['apellido'] }}</td>
              <td>{{ $volunteer['cedula'] }}</td>
              <td>{{ $volunteer['genero'] }}</td>
              <td>{{ $volunteer['email'] }}</td>
              <td>{{ $volunteer['telefono'] }}</td>
              <td>{{ $volunteer['nacimiento'] }}</td>
              <td>{{ $volunteer['parroquia'] }}</td>
              <td>{{ $volunteer['creacion'] }}</td>
          </tr>
      @endforeach
      </tbody>
    </table>
  </body>
</html>
